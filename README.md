# README #

## What is this repository for? ##

***A command line tool for playing around with obfuscation and encryption as well as decryption using the [Chaos-based](https://www.metacodes.pro/blog/chaos-based_encryption/) algorithm.***

## Usage ##

> See the [`CHAOS`](https://www.metacodes.pro/manpages/chaos_manpage) manpage for a complete user guide, basic usage instructions can be queried as follows:

```
$ ./teletype-chaos-x.y.z.sh --help
```

## Getting started ##

To get up and running, you use this archetype together with [`Maven`](https://maven.apache.org). Just change into the newly created folder and invoke `mvn` to build your new application:

```
cd funcodes-chaos
mvn clean install
```

This will package your application into a [`fatjar`](https://stackoverflow.com/questions/19150811/what-is-a-fat-jar) which can be executed:

```
java -jar ./target/funcodes-chaos-0.0.1.jar --help
```

When running a shell such as [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) then first (and once only) make sure the shell scripts are executable by changing the according file attributes and then invoke the `build.sh` script, so finally you can launch your application from inside the `target` folder:

```
chmod ug+x *.sh
```

> The `build.sh` script will finish off by printing out the actual path and the name of the created executable binary shell script.

```
./build.sh
./target/chaos-launcher-x.y.z.sh --help
```

> In case your Artifact-ID `funcodes-chaos` contains a slash ("-"), then just the portion after the slash is used for your executable binary shell script's name.

## Resources ##

* *[refcodes-cli: Parse your args[]](http://www.refcodes.org/refcodes/refcodes-cli)*
* *[org.refcodes:refcodes-cli@Bitbucket](https://bitbucket.org/refcodes/refcodes-cli)*

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-chaos/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

This code is written and provided by Siegfried Steiner, Munich, Germany. Feel free to use it as skeleton for your own applications. Make sure you have considered the license conditions of the included artifacts (see the provided `pom.xml` file).

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) artifacts used by this template are copyright (c) by Siegfried Steiner, Munich, Germany and licensed under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.