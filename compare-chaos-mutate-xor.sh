# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh [<imageFile> [<password>] ] [ -h ]"
	printLn
	echo -e "Creates a series of files using ${ESC_BOLD}<AES>${ESC_RESET} encryption from a base image to evaluate quality of encryption."
	printLn
	echo -e "${ESC_BOLD}<imageFile>${ESC_RESET}: The image file (${ESC_BOLD}PNG${ESC_RESET}) from which to create the series of files."
	echo -e " ${ESC_BOLD}<password>${ESC_RESET}: The password used for encryption/decryption."
	echo -e "         ${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

cd "${SCRIPT_PATH}"

ALGORITHM="chaos-mutate-xor"
sourceFolder="src/test/resources"
targetFolder="target"

printLn

echo "> Algorithm ........ = ${ALGORITHM}"
echo "> Source folder ...  = ${sourceFolder}"
echo "> Target folder ...  = ${targetFolder}"

printLn

imageA="test-A.png"
imageB="test-B.png"
imageC="test-C.png"

imagePathA="${sourceFolder}/${imageA}"
imagePathB="${sourceFolder}/${imageB}"
imagePathC="${sourceFolder}/${imageC}"

echo -e "> Image A: ... = $(realpath ${imagePathA})"
echo -e "> Image B: ... = $(realpath ${imagePathB})"
echo -e "> Image C: ... = $(realpath ${imagePathC})"

printLn

if [ ! -f "${imagePathA}" ]; then
    echo -e "> ${ESC_BOLD}No image <${imageA}> at <${sourceFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

if [ ! -f "${imagePathB}" ]; then
    echo -e "> ${ESC_BOLD}No image <${imageB}> at <${sourceFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

if [ ! -f "${imagePathC}" ]; then
    echo -e "> ${ESC_BOLD}No image <${imageC}> at <${sourceFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

verifyScript="verify-${ALGORITHM}.sh"
verifyCmd="${SCRIPT_PATH}/${verifyScript}"
compareCmd="compare"

echo "> Verify script: ..... = ${verifyScript}"
echo "> Compare command: ... = ${compareCmd}"

printLn

if ! which ${verifyCmd} &>/dev/null; then
	echo -e "> ${ESC_BOLD}Script <${verifyScript}> at <${SCRIPT_PATH}> not found!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

if ! which ${compareCmd} &>/dev/null; then
	echo -e "> ${ESC_BOLD}Command <${compareCmd}> not installed!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

encryptedA="test-A.png.3a.dat.${ALGORITHM}.png"
encryptedB="test-B.png.3a.dat.${ALGORITHM}.png"
encryptedC="test-C.png.3a.dat.${ALGORITHM}.png"
encryptedPathA="${targetFolder}/${encryptedA}"
encryptedPathB="${targetFolder}/${encryptedB}"
encryptedPathC="${targetFolder}/${encryptedC}"

if [ ! -f "${encryptedPathA}" ] || [ ! -f "${encryptedPathB}" ] || [ ! -f "${encryptedPathC}" ]; then
	if [ ! -f "${encryptedPathA}" ] ; then
		echo "> Encrypting and verifying <${imageA}> ..."
		result=$(${verifyCmd} "${imagePathA}")
		if [ $? -ne 0 ]; then
			printLn
			echo -e "> ${ESC_BOLD}Unable to encrrypt and verify <${imageA}>${ESC_RESET}"
			printLn
			echo "$result"
			cd "${CURRENT_PATH}"
			exit 1
		fi
	fi

	if [ ! -f "${encryptedPathB}" ] ; then
		echo "> Encrypting and verifying <${imageB}> ..."
		result=$(${verifyCmd} "${imagePathB}")
		if [ $? -ne 0 ]; then
			printLn
			echo -e "> ${ESC_BOLD}Unable to encrrypt and verify <${imageB}>${ESC_RESET}"
			printLn
			echo "$result"
			cd "${CURRENT_PATH}"
			exit 1
		fi
	fi

	if [ ! -f "${encryptedPathC}" ] ; then
		echo "> Encrypting and verifying <${imageC}> ..."
		result=$(${verifyCmd} "${imagePathC}")
		if [ $? -ne 0 ]; then
			printLn
			echo -e "> ${ESC_BOLD}Unable to encrrypt and verify <${imageC}>${ESC_RESET}"
			printLn
			echo "$result"
			cd "${CURRENT_PATH}"
			exit 1
		fi
	fi
	printLn
fi

echo "> Encrypted image A: ... = $(realpath ${encryptedPathA})"
echo "> Encrypted image B: ... = $(realpath ${encryptedPathB})"
echo "> Encrypted image C: ... = $(realpath ${encryptedPathC})"

printLn

if [ ! -f "${encryptedPathA}" ]; then
    echo -e "> ${ESC_BOLD}No encrypted image <${encryptedA}> at <${targetFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

if [ ! -f "${encryptedPathB}" ]; then
    echo -e "> ${ESC_BOLD}No encrypted image <${encryptedB}> at <${targetFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

if [ ! -f "${encryptedPathC}" ]; then
    echo -e "> ${ESC_BOLD}No encrypted image <${encryptedC}> at <${encryptedFolder}> found!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi


compareAB="compare-${encryptedA}-${encryptedB}"
compareAC="compare-${encryptedA}-${encryptedC}"
compareBC="compare-${encryptedB}-${encryptedC}"
comparePathAB="${targetFolder}/${compareAB}"
comparePathAC="${targetFolder}/${compareAC}"
comparePathBC="${targetFolder}/${compareBC}"

echo "> Compare A <-> B: ... = $(realpath ${comparePathAB})"
result=$(compare -compose src "${encryptedPathA}" "${encryptedPathB}" "${comparePathAB}")
if [ $? -gt 1 ]; then
	printLn
	echo -e "> ${ESC_BOLD}Unable to compare <${encryptedA}> and <${encryptedB}> in <${targetFolder}>${RESET}"
	printLn
	echo "$result"
	cd "${CURRENT_PATH}"
	exit 1
fi

echo "> Compare A <-> C: ... = $(realpath ${comparePathAC})"
result=$(compare -compose src "${encryptedPathA}" "${encryptedPathC}" "${comparePathAC}")
if [ $? -gt 1 ]; then
	printLn
	echo -e "> ${ESC_BOLD}Unable to compare <${encryptedA}> and <${encryptedC}> in <${targetFolder}>${RESET}"
	printLn
	echo "$result"
	cd "${CURRENT_PATH}"
	exit 1
fi

echo "> Compare B <-> C: ... = $(realpath ${comparePathBC})"
result=$(compare -compose src "${encryptedPathB}" "${encryptedPathC}" "${comparePathBC}")
if [ $? -gt 1  ]; then
	printLn
	echo -e "> ${ESC_BOLD}Unable to compare <${encryptedB}> and <${encryptedC}> in <${targetFolder}>${RESET}"
	printLn
	echo "$result"
	cd "${CURRENT_PATH}"
	exit 1
fi

printLn

cd "${CURRENT_PATH}"
echo "> Done!"
