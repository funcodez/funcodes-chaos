# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh [<imageFile> [<certFile> <password> ] ] [ -h ]"
	printLn
	echo -e "Creates a series of files using ${ESC_BOLD}<CHAOS>${ESC_RESET} encryption from a base image to evaluate quality of encryption."
	printLn
	echo -e "${ESC_BOLD}<imageFile>${ESC_RESET}: The image file (${ESC_BOLD}PNG${ESC_RESET}) from which to create the series of files."
	echo -e " ${ESC_BOLD}<certFile>${ESC_RESET}: The certificate file to use."
	echo -e " ${ESC_BOLD}<password>${ESC_RESET}: The password used for decryption of the <certFile>."
	echo -e "         ${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

ALGORITHM="chaos-cert"

imageFile="$1"
if [ -z "${1}" ]; then
	printHelp
	echo -e "> ${ESC_BOLD}No <imageFile> has been provided!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi
if [ ! -f "${1}" ]; then
	printHelp
	echo -e "> ${ESC_BOLD}The <imageFile> at <${1}> does not exist!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

baseName="$(basename ${imageFile})"
targetBasePath="${SCRIPT_PATH}/target/${baseName}"
unameOut="$(uname -s)"
case "${unameOut}" in
    CYGWIN*) targetBasePath="$(cygpath -wa "${targetBasePath}")";;
esac

if [[ "${baseFile}" == "${imageFile}" ]]; then
	echo -e "> ${ESC_BOLD}The <imageFile> must have the file extension \".png\"!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

info="$(file "${imageFile}" | cut -f 2 -d ',' | tr -d '[:space:]' )"

width="${info%%x*}"
if ! [ "$width" -eq "$width" ] 2> /dev/null; then
	echo -e "> ${ESC_BOLD}Cannot determine width for <${imageFile}>!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi
height="${info#*x}"
if ! [ "$width" -eq "$width" ] 2> /dev/null; then
	echo -e "> ${ESC_BOLD}Cannot determine height for <${imageFile}>!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

width=$(echo -e "${width}" | tr -d '[:space:]')
height=$(echo -e "${height}" | tr -d '[:space:]')
printLn
echo "> Algorithm ...... = ${ALGORITHM}"
echo "> Image file ..... = ${imageFile}"
echo "> Image info ..... = ${info}"
echo "> Image width .... = ${width}"
echo "> Image height ... = ${height}"
echo "> Target files ... = ${targetBasePath}-*"

chaosScriptPath="${SCRIPT_PATH}/target"
chaosScriptPath="$(realpath "${chaosScriptPath}")"
chaos=$(find "${chaosScriptPath}" -type f -name "chaos-launcher-*.sh")
if [ ! -f "${chaos}" ]; then
    echo -e "> ${ESC_BOLD}No CHAOS launcher found (<${chaos}>), make sure you scriptified your according project!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

if [[ ! -z "$2" ]] ; then
	echo "> Cert file ........ = ${3}"
	certFile="${2}"
else
	certFile="sample.cert"
fi
if [ ! -f "${certFile}" ]; then
	printHelp
	echo -e "> ${ESC_BOLD}The <certFile> at <${certFile}> does not exist!${ESC_RESET}"
	cd "${CURRENT_PATH}"
	exit 1
fi

if [[ ! -z "$3" ]] ; then
	echo "> Using password ... = ${3}"
	password="${3}"
else
	password="Secret123!"
fi

chaosKeyLength=$("${chaos}" --encoded-length)
chaosKeyLength=${chaosKeyLength//$'\x15'/}
chaosKeyLength=${chaosKeyLength//$'\x0D'/}

echo "> Prefix bytes ... = ${chaosKeyLength}"

dadumScriptPath="${SCRIPT_PATH}/../funcodes-dadum/target"
dadumScriptPath="$(realpath "${dadumScriptPath}")"
dadum=$(find "${dadumScriptPath}" -type f -name "dadum-launcher-*.sh")
if [ ! -f "${dadum}" ]; then
    printLn
    echo -e "> ${ESC_BOLD}No DADUM launcher found (<${dadum}>), make sure you scriptified your according project!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi


picdatScriptPath="${SCRIPT_PATH}/../funcodes-picdat/target"
picdatScriptPath="$(realpath "${picdatScriptPath}")"
picdat=$(find "${picdatScriptPath}" -type f -name "picdat-launcher-*.sh")
if [ ! -f "${picdat}" ]; then
    printLn
    echo -e "> ${ESC_BOLD}No PICDAT launcher found (<${picdat}>), make sure you scriptified your according project!${ESC_RESET}"
    cd "${CURRENT_PATH}"
	exit 1
fi

printLn
echo "> Using CHAOS launcher .... = ${chaos}"
echo "> Using DADUM launcher .... = ${dadum}"
echo "> Using PICDAT launcher ... = ${picdat}"

printLn
currentFileA="${imageFile}"
currentFileB="${targetBasePath}.1.dat"
echo -n "> Creating DAT file at ..................... : ${currentFileB}"
"${picdat}" -e -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

currentFileA="${currentFileB}"
currentFileB="${targetBasePath}.2a.dat.png"
echo -n "> Creating PNG file (from DAT file) at ..... : ${currentFileB}"
"${picdat}"  -c --width ${width} --height ${height} -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

currentFileB="${targetBasePath}.2b.dat.${ALGORITHM}"
echo -n "> Creating CHAOS file (from DAT file) at ... : ${currentFileB}"
"${chaos}" -p "${password}" --cert-file "${certFile}" -e -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

currentFileA="${currentFileB}"
currentFileB="${targetBasePath}.3a.dat.${ALGORITHM}.png"
echo -n "> Creating PNG file (from CHAOS file) at ... : ${currentFileB}"
"${picdat}" --offset ${chaosKeyLength} -c --width ${width} --height ${height} -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

currentFileB="${targetBasePath}.3b.dat.${ALGORITHM}.dat"
echo -n "> Creating DAT file (from CHAOS file) at ... : ${currentFileB}"
"${chaos}" -p "${password}" --cert-file "${certFile}" -d -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

currentFileA="${currentFileB}"
currentFileB="${targetBasePath}.4.dat.${ALGORITHM}.dat.png"
echo -n "> Creating PNG file (from DAT file) at ..... : ${currentFileB}"
"${picdat}" -c --width ${width} --height ${height} -i "${currentFileA}" -o "${currentFileB}"
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Unable to process <${currentFileA}> to <${currentFileB}>!"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"

printLn
currentFileA="${targetBasePath}.2a.dat.png"
currentFileB="${targetBasePath}.4.dat.${ALGORITHM}.dat.png"
echo -n "> Validating decrypted <${targetBasePath}.*.dat.png> files (diff) with each other ..."
result=$(diff "${currentFileA}" "${currentFileB}")
if [ $? -ne 0 ]; then
	echo -e " [${ESC_FG_RED}ERROR${ESC_RESET}]"
	printLn
	echo "> Files <${currentFileA}> and <${currentFileB}> are different!"
	echo "> ${result}"
	cd "${CURRENT_PATH}"
	exit 1
fi
echo -e " [${ESC_FG_GREEN}OK${ESC_RESET}]"
printLn

if which ent &>/dev/null; then
	currentFileA="${targetBasePath}.1.dat"
	currentFileB="${targetBasePath}.2b.dat.${ALGORITHM}"
	echo -e "> Entropy ${ESC_BOLD}bits/byte${ESC_RESET} (8 = best) of <${currentFileA}> = ................... ${ESC_BOLD}$(ent "${currentFileA}" | head -1 | cut -d ' ' -f3)${ESC_RESET}"
	echo -e "> Entropy ${ESC_BOLD}bits/byte${ESC_RESET} (8 = best) of <${currentFileB}> = ....... ${ESC_BOLD}$(ent "${currentFileB}" | head -1 | cut -d ' ' -f3)${ESC_RESET}"
	echo -e "> Arithmetic ${ESC_BOLD}mean${ESC_RESET} (127.5 = random) of <${currentFileA}> = ............... ${ESC_BOLD}$(ent -t "${currentFileA}" | cut -d$'\n' -f2 | cut -d , -f 5)${ESC_RESET}"
	echo -e "> Arithmetic ${ESC_BOLD}mean${ESC_RESET} (127.5 = random) of <${currentFileB}> = ... ${ESC_BOLD}$(ent -t "${currentFileB}" | cut -d$'\n' -f2 | cut -d , -f 5)${ESC_RESET}"
	echo -e "> Serial ${ESC_BOLD}correlation${ESC_RESET} (none = 0.0) of <${currentFileA}> = ................ ${ESC_BOLD}$(ent -t "${currentFileA}" | cut -d$'\n' -f2 | cut -d , -f 7)${ESC_RESET}"
	echo -e "> Serial ${ESC_BOLD}correlation${ESC_RESET} (none = 0.0) of <${currentFileB}> = .... ${ESC_BOLD}$(ent -t "${currentFileB}" | cut -d$'\n' -f2 | cut -d , -f 7)${ESC_RESET}"
	echo -e "> Max ${ESC_BOLD}compression${ESC_RESET} (0% = none) of <${currentFileA}> = .................... ${ESC_BOLD}$(ent "${currentFileA}" | grep -o -P '(?<=by ).*(?= percent)')${ESC_RESET}%"
	echo -e "> Max ${ESC_BOLD}compression${ESC_RESET} (0% = none) of <${currentFileB}> = ........ ${ESC_BOLD}$(ent "${currentFileB}" | grep -o -P '(?<=by ).*(?= percent)')${ESC_RESET}%"
	printLn
fi

echo "> Done!"
