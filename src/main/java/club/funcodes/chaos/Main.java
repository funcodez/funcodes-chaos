// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.chaos;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.tabular.TabularSugar.*;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Condition;
import org.refcodes.cli.DoubleOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.LongOption;
import org.refcodes.cli.StringOption;
import org.refcodes.codec.BaseBuilder;
import org.refcodes.codec.BaseDecoderInputStream;
import org.refcodes.codec.BaseEncoderOutputStream;
import org.refcodes.codec.BaseMetrics;
import org.refcodes.codec.BaseMetricsConfig;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Delimiter;
import org.refcodes.data.ExitCode;
import org.refcodes.exception.BugException;
import org.refcodes.io.ClipboardOutputStream;
import org.refcodes.io.LineBreakOutputStream;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemContext;
import org.refcodes.security.DecryptionException;
import org.refcodes.security.DecryptionInputStream;
import org.refcodes.security.EncryptionException;
import org.refcodes.security.EncryptionOutputStream;
import org.refcodes.security.alt.chaos.ChaosDecryptionInputStream;
import org.refcodes.security.alt.chaos.ChaosEncryptionOutputStream;
import org.refcodes.security.alt.chaos.ChaosKey;
import org.refcodes.security.alt.chaos.ChaosMode;
import org.refcodes.security.alt.chaos.ChaosOptions;
import org.refcodes.security.alt.chaos.ChaosOptionsImpl;
import org.refcodes.tabular.CsvRecordReader;
import org.refcodes.tabular.Header;
import org.refcodes.textual.CaseStyle;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.SecretHintBuilder;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final int MIN_LINE_WIDTH = 65;

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "chaos";
	private static final String TITLE = ">?" + NAME.toUpperCase() + "!>";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Credits [C] Sönke Rehder | Copyright (c) by FUNCODES.CLUB | See [https://www.metacodes.pro/manpages/chaos_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Encryption and decryption tool of file or stream data using Chaos-based encryption (see [https://www.metacodes.pro/manpages/chaos_manpage]).";
	private static final String BASE64_PROPERTY = "base64";
	private static final String BYTES_PROPERTY = "bytes";
	private static final String COPY_TO_PROPERTY = "copyTo";
	private static final String PASTE_FROM_PROPERTY = "pasteFrom";
	private static final String CERT_FILE_PROPERTY = "certFile";
	private static final String CERT_INFO_PROPERTY = "certInfo";
	private static final String CERT_MODE_PROPERTY = "certMode";
	private static final String CHAIN_LENGTH_PROPERTY = "chainLength";
	private static final String CHAOS_MODE_PROPERTY = "chaosMode";
	private static final String CONTEXT_PROPERTY = "context";
	private static final String CREATE_CERT_PROPERTY = "createCert";
	private static final String CREATE_SPEC_PROPERTY = "createSpec";
	private static final String DECRYPT_PROPERTY = "decrypt";
	private static final String ENCODED_LENGTH_PROPERTY = "encodedLength";
	private static final String ENCODING_PROPERTY = "encoding";
	private static final String ENCRYPT_PROPERTY = "encrypt";
	private static final String HEX_PROPERTY = "hex";
	private static final String INPUT_FILE_PROPERTY = "inputFile";
	private static final String LINE_WIDTH_PROPERTY = "lineWidth";
	private static final String OUTPUT_FILE_PROPERTY = "outputFile";
	private static final String PASSWORD_PROPERTY = "password";
	private static final String PROMPT_PASSWORD_PROPERTY = "promptPassword";
	private static final String SPEC_FILE_PROPERTY = "specFile";
	private static final String TEXT_PROPERTY = "text";
	private static final String VERIFY_PROPERTY = "verify";
	private static final String X0_PROPERTY = "x0";
	private static final String A_PROPERTY = "a";
	private static final String S_PROPERTY = "s";
	private static final ChaosMode DEFAULT_CHAOS_MODE = ChaosMode.SALTED_MUTATE;
	private static final ChaosMode DEFAULT_PLAIN_MODE = ChaosMode.NONE;
	private static final String DEFAULT_SPECFILE_NAME = "sample-chaos.spec";
	private static final SystemContext DEFAULT_CONTEXT = SystemContext.HOST_USER;
	private static final int DEFAULT_CERT_LINE_WIDTH = 65;
	private static final int DEFAULT_CHAIN_LENGTH = 16;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, ENCODING_PROPERTY, "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final EnumOption<ChaosMode> theCertModeArg = enumOption( "cert-mode", ChaosMode.class, CERT_MODE_PROPERTY, "The chaos options (mode) to be used for the cert itself: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<ChaosMode> theChaosModeArg = enumOption( "chaos-mode", ChaosMode.class, CHAOS_MODE_PROPERTY, "The mode to be used when encrypting/decrypting: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<SystemContext> theContextArg = enumOption( "context", SystemContext.class, CONTEXT_PROPERTY, "The system context providing the password: " + VerboseTextBuilder.asString( SystemContext.values() ) );
		final Flag theBase64Flag = flag( "base64", BASE64_PROPERTY, "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theCertInfoFlag = flag( "cert-info", CERT_INFO_PROPERTY, "Prints out information of a given cert." );
		final Flag theCreateCertFlag = flag( "create-cert", CREATE_CERT_PROPERTY, "Create an according cert record (file)." );
		final Flag theCreateSpecFlag = flag( "create-spec", CREATE_SPEC_PROPERTY, "Create an according spec record (file)." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theDecryptFlag = flag( 'd', "decrypt", DECRYPT_PROPERTY, "Decrypts the message (stream, file, text or bytes)." );
		final Flag theEncodedFlag = flag( "encoded-length", ENCODED_LENGTH_PROPERTY, "Prints length in bytes of a single chaos key encoding (being the additional length of a salted encryption)." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", ENCRYPT_PROPERTY, "Encrypts the message (stream, file, text or bytes)." );
		final Flag theHelpFlag = helpFlag();
		final Flag theHexFlag = flag( "hex", HEX_PROPERTY, "Use a hexadecimal representation of (binary) output." );
		final Flag thePromptPasswordFlag = flag( "prompt", PROMPT_PASSWORD_PROPERTY, "Prompt for the password to use for encryption or decryption." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theVerifyFlag = flag( "verify", VERIFY_PROPERTY, "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final Flag theCopyToFlag = flag( "copy-to", COPY_TO_PROPERTY, "Copy the processed output data to the clipboard" + ( Execution.isNativeImage() ? " (without warranty)" : "" ) + "." );
		final Flag thePasteFromFlag = flag( "paste-from", PASTE_FROM_PROPERTY, "Paste the input data to be processed from the clipboard" + ( Execution.isNativeImage() ? " (without warranty)" : "" ) + "." );
		final IntOption theChainLengthArg = intOption( "chain-length", CHAIN_LENGTH_PROPERTY, "The length of the chaos key chain (e.g. number of nested chaos keys, defaults to <" + DEFAULT_CHAIN_LENGTH + ">)" );
		final IntOption theLineWidthArg = intOption( "line-width", LINE_WIDTH_PROPERTY, "The line width for base64 encoded ASCII output." );
		final DoubleOption theX0Arg = doubleOption( 'x', "start-value", X0_PROPERTY, "The chaos key's <x0> start value to use: " + ChaosKey.X_MIN + " < x0 ≤ " + ChaosKey.X_MAX );
		final DoubleOption theAArg = doubleOption( 'a', "parable-coefficient", A_PROPERTY, "The chaos key's parable coefficient <a> to use: " + ChaosKey.A_MIN + " ≤ a ≤ " + ChaosKey.A_MAX );
		final LongOption theSArg = longOption( 's', "expansion-factor", S_PROPERTY, "The chaos key's expansion factor <s> to use: " + ChaosKey.S_MIN + " ≤ s ≤ " + ChaosKey.S_NEGATIVE_MAX + " or " + ChaosKey.S_POSITIVE_MIN + " ≤ s ≤ " + ChaosKey.S_MAX );
		final StringOption theBytesArg = stringOption( 'b', "bytes", BYTES_PROPERTY, "The message in bytes (e.g. \"127, 128, 0x10, 0xFF\") which to process." );
		final StringOption theCertFileArg = stringOption( 'c', "cert-file", CERT_FILE_PROPERTY, "The cert file file which to use." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", INPUT_FILE_PROPERTY, "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", OUTPUT_FILE_PROPERTY, "The output file which to process to." );
		final StringOption thePasswordArg = stringOption( 'p', "password", PASSWORD_PROPERTY, "The password to use for encryption or decryption." );
		final StringOption theSpecFileArg = stringOption( "spec-file", SPEC_FILE_PROPERTY, "The spec file file which to use." );
		final StringOption theTextArg = stringOption( 't', "text", TEXT_PROPERTY, "The text message which to process." );

		// @formatter:off
		final Condition theArgsSyntax = cases(
			// Text + Bytes
			and( // Encrypt:
				theEncryptFlag, 
				xor( theTextArg, theBytesArg ),
				any(
					xor( theHexFlag, theBase64Flag, theEncodingArg ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) )
					),
					theCopyToFlag, theVerifyFlag, theVerboseFlag, theDebugFlag
				)
			),
			and( // Decrypt: 
				theDecryptFlag, 
				 xor( theTextArg, theBytesArg ),
				 any(
					xor( theBase64Flag, theEncodingArg ),  theHexFlag,
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) )
					),
					theCopyToFlag, theVerboseFlag, theDebugFlag
				)
			),
			// Input + Output
			and( // Encrypt
				theEncryptFlag, 
				any(
					xor( theInputFileArg, thePasteFromFlag ), xor( theOutputFileArg, theCopyToFlag ),
					and( xor( theBase64Flag, theEncodingArg ), any ( theLineWidthArg ) ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ), 
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) ) 
					),
					theVerifyFlag, theVerboseFlag, theDebugFlag
				)
			),
			and( // Decrypt
				 theDecryptFlag, 
				any(
					xor( theInputFileArg, thePasteFromFlag ), xor( theOutputFileArg, theCopyToFlag ),
					xor( theBase64Flag, theEncodingArg ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ), 
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) ) 
					),
					theVerboseFlag, theDebugFlag
				)
			),
			// CERT
			and( 
				theCreateCertFlag, any ( theLineWidthArg, theCertFileArg, and( xor( thePromptPasswordFlag, thePasswordArg, theContextArg ), any( theCertModeArg ) ), xor( or( theChainLengthArg, theChaosModeArg), theSpecFileArg ), theVerboseFlag, theDebugFlag ) 
			),
			and(
				theCertInfoFlag, theCertFileArg, any(  xor( thePromptPasswordFlag, thePasswordArg, theContextArg ), theVerboseFlag, theDebugFlag )
			),
			and( 
				theCreateSpecFlag, any ( theSpecFileArg , theVerboseFlag, theDebugFlag ) 
			),
			xor(
				theHelpFlag, and( xor( theEncodedFlag, theSysInfoFlag ), any ( theVerboseFlag, theDebugFlag ) )
			)
		);

		final Example[] theExamples = examples(
			example( "Encrypt (BASE64 encoded) a text message", theEncryptFlag, theTextArg, theBase64Flag, thePromptPasswordFlag), 
			example( "Decrypt a text message (BASE64 encoded)", theDecryptFlag, theTextArg, theBase64Flag, thePromptPasswordFlag ),
			example( "Encrypt (BASE64 encoded) a text message and copy to clipboard", theEncryptFlag, theTextArg, theBase64Flag, thePromptPasswordFlag, theCopyToFlag), 
			example( "Decrypt a text message (BASE64 encoded) and copy to clipboard", theDecryptFlag, theTextArg, theBase64Flag, thePromptPasswordFlag, theCopyToFlag ),
			// example( "Encrypt (BASE64 encoded) the clipboard", theEncryptFlag, theBase64Flag, thePromptPasswordFlag, thePasteFromFlag), 
			// example( "Decrypt (BASE64 encoded) the clipboard", theDecryptFlag, theBase64Flag, thePromptPasswordFlag, thePasteFromFlag ),
			example( "Encrypt (BASE64 encoded) a text message using (x0, a, s)", theEncryptFlag, theTextArg, theBase64Flag, theX0Arg, theAArg, theSArg), 
			example( "Decrypt a text message (BASE64 encoded) using (x0, a, s)", theDecryptFlag, theTextArg, theBase64Flag, theX0Arg, theAArg, theSArg ),
			example( "Encrypt a text message with mode", theEncryptFlag, theTextArg, theVerboseFlag, theChaosModeArg ), 
			example( "Decrypt a text message with mode", theDecryptFlag, theTextArg, theVerboseFlag, theChaosModeArg ),
			example( "Encrypt a message in bytes", theEncryptFlag, theBytesArg, theHexFlag, thePromptPasswordFlag ), 
			example( "Decrypt a message in bytes", theDecryptFlag, theBytesArg, theHexFlag, thePromptPasswordFlag ),
			example( "Encrypt input file to output file (bound to context)", theEncryptFlag, theInputFileArg, theOutputFileArg, theContextArg, theVerboseFlag ), 
			example( "Decrypt input file to output file (bound to context)", theDecryptFlag, theInputFileArg, theOutputFileArg, theContextArg, theVerboseFlag ),
			example( "Use password to encrypt input file to output file", theEncryptFlag, theInputFileArg, theOutputFileArg, thePasswordArg, theVerboseFlag ), 
			example( "Use password to decrypt input file to output file", theDecryptFlag, theInputFileArg, theOutputFileArg, thePasswordArg, theVerboseFlag ),
			example( "Encrypt input file to output file, prompt for password", theEncryptFlag, theInputFileArg, theOutputFileArg, thePromptPasswordFlag, theVerboseFlag ), 
			example( "Decrypt input file to output file, prompt for password", theDecryptFlag, theInputFileArg, theOutputFileArg, thePromptPasswordFlag, theVerboseFlag ),
			example( "Encrypt clipboard to output file, prompt for password", theEncryptFlag, thePasteFromFlag, theOutputFileArg, thePromptPasswordFlag, theVerboseFlag ), 
			example( "Decrypt input file to clipboard, prompt for password", theDecryptFlag, theInputFileArg, theCopyToFlag, thePromptPasswordFlag, theVerboseFlag ),
			example( "Encrypt input file to output file with mode and password", theEncryptFlag, theInputFileArg, theOutputFileArg, theChaosModeArg, thePasswordArg, theVerboseFlag ), 
			example( "Decrypt input file to output file with mode and password", theDecryptFlag, theInputFileArg, theOutputFileArg, theChaosModeArg, thePasswordArg, theVerboseFlag ),
			example( "Encrypt STDIN to output file with password", theEncryptFlag, theInputFileArg, thePasswordArg ), 
			example( "Decrypt input file to STDOUT with password", theDecryptFlag, theOutputFileArg, thePasswordArg ),
			example( "Encrypt STDIN to output file with mode for " + DEFAULT_CONTEXT, theEncryptFlag, theInputFileArg, theChaosModeArg ), 
			example( "Decrypt input file to STDOUT with mode for " + DEFAULT_CONTEXT, theDecryptFlag, theOutputFileArg, theChaosModeArg ),
			example( "Encrypt STDIN to STDOUT/BASE64 encoded for " + DEFAULT_CONTEXT, theEncryptFlag, theBase64Flag),
			example( "Decrypt STDIN/BASE64 encoded to STDOUT for " + DEFAULT_CONTEXT, theDecryptFlag, theBase64Flag ),
			example( "Create and print cert with key chain length", theCreateCertFlag, theChainLengthArg, theVerboseFlag),
			example( "Create cert file with key chain length", theCreateCertFlag, theCertFileArg, theChainLengthArg, theVerboseFlag),
			example( "Create secured cert file with key chain length and mode", theCreateCertFlag, theCertFileArg, theChainLengthArg, theChaosModeArg, thePromptPasswordFlag, theVerboseFlag),
			example( "Create sample cert spec file (for you to edit)", theCreateSpecFlag, theSpecFileArg, theVerboseFlag),
			example( "Create secured cert file from (edited) cert spec file", theCreateCertFlag, theCertFileArg, theSpecFileArg, thePromptPasswordFlag, theVerboseFlag),
			example( "Encrypt input file to output file, use secured cert", theEncryptFlag, theInputFileArg, theOutputFileArg, theCertFileArg, thePromptPasswordFlag, theVerboseFlag ), 
			example( "Decrypt input file to output file, use secured cert", theDecryptFlag, theInputFileArg, theOutputFileArg, theCertFileArg, thePromptPasswordFlag, theVerboseFlag ),
			example( "Print cert file info", theCertInfoFlag, theCertFileArg, theVerboseFlag),
			example( "Print secured cert file info", theCertInfoFlag, theCertFileArg, thePromptPasswordFlag, theVerboseFlag),
			example( "Print length of single chaos key (bytes)", theEncodedFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);

		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();

		if ( theArgsProperties.getBoolean( theEncodedFlag ) ) {
			printEncodedLength( isVerbose );
		}
		else {
			try {
				final byte[] theBytes = theArgsProperties.get( theBytesArg ) != null ? NumericalUtility.toBytes( theArgsProperties.get( theBytesArg ) ) : null;
				final String theText = theArgsProperties.get( theTextArg );
				final String theInputFilePath = theArgsProperties.get( theInputFileArg );
				final String theOutputFilePath = theArgsProperties.get( theOutputFileArg );
				final String theCertFileName = theArgsProperties.get( theCertFileArg );
				final String theSpecFileName = theArgsProperties.get( theSpecFileArg );
				final Boolean isPasteFrom = theArgsProperties.getBoolean( thePasteFromFlag );
				final Boolean isCopyTo = theArgsProperties.getBoolean( theCopyToFlag );
				final Boolean isBase64 = theArgsProperties.getBoolean( theBase64Flag );
				final Boolean isHex = theArgsProperties.getBoolean( theHexFlag );
				final Boolean isEncrypt = theArgsProperties.getBoolean( theEncryptFlag );
				final Boolean isDecrypt = theArgsProperties.getBoolean( theDecryptFlag );
				final Boolean isVerify = theArgsProperties.getBoolean( theVerifyFlag );
				final Boolean isCreateCert = theArgsProperties.getBoolean( theCreateCertFlag );
				final Boolean isCertInfo = theArgsProperties.getBoolean( theCertInfoFlag );
				final Boolean isCreateSpec = theArgsProperties.getBoolean( theCreateSpecFlag );
				final Boolean isPromptPassword = theArgsProperties.getBoolean( thePromptPasswordFlag );
				final String thePassword = theArgsProperties.get( thePasswordArg );
				final SystemContext theContext = theArgsProperties.getEnum( SystemContext.class, theContextArg );
				final BaseMetricsConfig theBaseMetrics = isBase64 ? BaseMetricsConfig.BASE64 : theArgsProperties.getEnum( BaseMetricsConfig.class, theEncodingArg );
				final Double x0 = theArgsProperties.getDouble( theX0Arg );
				final Double a = theArgsProperties.getDouble( theAArg );
				final Long s = theArgsProperties.getLong( theSArg );
				Integer theLineWidth = theArgsProperties.getInt( theLineWidthArg );
				Integer theChainLength = theArgsProperties.getInt( theChainLengthArg );
				ChaosOptions theChaosMode = theArgsProperties.getEnum( ChaosMode.class, theChaosModeArg );
				ChaosOptions theCertMode = theArgsProperties.getEnum( ChaosMode.class, theCertModeArg );

				if ( isVerbose ) {
					if ( isCreateCert != null && isCreateCert ) {
						LOGGER.info( "Operation = " + CaseStyle.KEBAB_UPPER_CASE.toCaseStyle( CREATE_CERT_PROPERTY ) );
					}
					if ( isCertInfo != null && isCertInfo ) {
						LOGGER.info( "Operation = " + CaseStyle.KEBAB_UPPER_CASE.toCaseStyle( CERT_INFO_PROPERTY ) );
					}
					if ( isDecrypt != null && isDecrypt ) {
						LOGGER.info( "Operation = " + CaseStyle.KEBAB_UPPER_CASE.toCaseStyle( DECRYPT_PROPERTY ) );
					}
					if ( isEncrypt != null && isEncrypt ) {
						LOGGER.info( "Operation = " + CaseStyle.KEBAB_UPPER_CASE.toCaseStyle( ENCRYPT_PROPERTY ) );
					}
					if ( x0 != null ) {
						LOGGER.info( "x0 = " + x0 );
					}
					if ( a != null ) {
						LOGGER.info( "a = " + a );
					}
					if ( s != null ) {
						LOGGER.info( "s = " + s );
					}
					if ( thePassword != null ) {
						LOGGER.info( "Password (hint) = " + SecretHintBuilder.asString( thePassword ) );
					}
					if ( isPromptPassword != null && isPromptPassword ) {
						LOGGER.info( "Prompt password = " + toBooleanText( isPromptPassword ) );
					}
					if ( theContext != null ) {
						LOGGER.info( "Context = " + theContext.name() );
					}
					if ( theText != null && theText.length() != 0 ) {
						LOGGER.info( "Text = " + theText );
					}
					if ( theBytes != null && theBytes.length != 0 ) {
						LOGGER.info( "Bytes = { " + NumericalUtility.toHexString( ", ", theBytes ) + " }" );
					}
					if ( theBaseMetrics != null ) {
						LOGGER.info( "Encoding = " + theBaseMetrics );
					}
					if ( isHex != null && isHex ) {
						LOGGER.info( "Hexadecimal = " + toBooleanText( isHex ) );
					}
					if ( isVerify != null && isVerify ) {
						LOGGER.info( "Verify = " + toBooleanText( isVerify ) );
					}
					if ( isCopyTo != null && isCopyTo ) {
						LOGGER.info( "Copy to clipboard = " + toBooleanText( isCopyTo ) );
					}
					if ( isPasteFrom != null && isPasteFrom ) {
						LOGGER.info( "Past from clipboard = " + toBooleanText( isPasteFrom ) );
					}
				}

				if ( isCreateCert ) {
					final File theSpecFile = toSpecFile( theSpecFileName, isVerbose );
					final File theCertFile = toCertFile( theCertFileName, isVerbose );
					final String theCertPassword = toCertPassword( isPromptPassword, thePassword, theContext, isVerbose );
					theLineWidth = toLineWidth( theLineWidth, DEFAULT_CERT_LINE_WIDTH, isVerbose );
					theChaosMode = toChaosMode( theChaosMode, isVerbose );
					theChainLength = toChainLength( theChainLength, theSpecFile, isVerbose );
					theCertMode = toCertMode( isPromptPassword, thePassword, theContext, theCertMode, isVerbose );
					createCert( theCertFile, theSpecFile, theChaosMode, theChainLength, theCertMode, theCertPassword, theLineWidth, isVerbose );
				}
				else if ( isCertInfo ) {
					printCertInfo( theCertFileName, isPromptPassword, thePassword, theContext, isVerbose );
				}
				else if ( isCreateSpec ) {
					final File theSpecFile = toSpecFile( toSpecFileName( theSpecFileName ), isVerbose );
					createSpecFile( theSpecFile, isVerbose );
				}
				else if ( isEncrypt ) {
					final File theCertFile = toCertFile( theCertFileName, isVerbose );
					try ( InputStream theInputStream = toInputStream( isPasteFrom, theInputFilePath, isVerbose ); OutputStream theOutputStream = toOutputStream( isCopyTo, theOutputFilePath, isVerbose ) ) {
						theChaosMode = toChaosMode( theChaosMode, theText, theBytes, isPasteFrom, theCertFile, isVerbose );
						final ChaosKey theChaosKey = toChaosKey( isPromptPassword, thePassword, theContext, theCertFile, x0, a, s, theChaosMode, isVerbose );
						theLineWidth = toLineWidth( theLineWidth, -1, isVerbose );
						encrypt( theText, theBytes, theInputStream, theOutputStream, theChaosKey, isVerify, isHex, theBaseMetrics, theLineWidth, isVerbose );
					}
				}
				else if ( isDecrypt ) {
					final File theCertFile = toCertFile( theCertFileName, isVerbose );
					try ( InputStream theInputStream = toInputStream( isPasteFrom, theInputFilePath, isVerbose ); OutputStream theOutputStream = toOutputStream( isCopyTo, theOutputFilePath, isVerbose ) ) {
						theChaosMode = toChaosMode( theChaosMode, theText, theBytes, isPasteFrom, theCertFile, isVerbose );
						final ChaosKey theChaosKey = toChaosKey( isPromptPassword, thePassword, theContext, theCertFile, x0, a, s, theChaosMode, isVerbose );
						decrypt( theText, theBytes, theInputStream, theOutputStream, theChaosKey, isHex, theBaseMetrics, isVerbose );
					}
				}
				else {
					throw new BugException( "We encountered a bug, none argument was processed!" );
				}
			}
			catch ( Throwable e ) {
				theCliHelper.exitOnException( e );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void printCertInfo( String aCertFileName, boolean isPromptPassword, String aPassword, SystemContext aContext, boolean isVerbose ) throws IOException {
		final File theCertFile = toCertFile( aCertFileName, isVerbose );
		final String theFileSize = "Cert file size  = " + theCertFile.length();
		final ChaosOptions theChaosOptions = ChaosKey.asCertificateOptions( theCertFile );
		final String theCertOptions = "Cert file options = " + ( theChaosOptions != null ? "Encrypted: " + theChaosOptions.toString() : "None" );
		final ChaosKey theChaosKey = toChaosKey( isPromptPassword, aPassword, aContext, theCertFile, null, null, null, null, isVerbose );
		if ( isVerbose ) {
			LOGGER.info( theFileSize );
			LOGGER.info( theCertOptions );
			LOGGER.printSeparator();
			final int length = printChaosKey( theChaosKey, LOGGER::info, true );
			LOGGER.printSeparator();
			LOGGER.info( "Chain length = " + length );

		}
		else {
			System.out.println( theFileSize );
			System.out.println( theCertOptions );
			final int length = printChaosKey( theChaosKey, System.out::println, false );
			System.out.println( "Chain length = " + length );
		}
	}

	private static void printEncodedLength( boolean isVerbose ) {
		if ( isVerbose ) {
			LOGGER.info( "Encoded chaos key length (bytes) = " + ChaosKey.getEncodedLength() );
		}
		else {
			System.out.println( ChaosKey.getEncodedLength() );
		}
	}

	private static void createCert( File aCertFile, File aSpecFile, ChaosOptions aChaosMode, Integer aChainLength, ChaosOptions aCertMode, String aPassword, int aLineWidth, boolean isVerbose ) throws IOException {
		final Header<?> theHeader = headerOf( stringColumn( "x0" ), stringColumn( "a" ), stringColumn( "s" ), booleanColumn( "salted" ), booleanColumn( "mutate" ), booleanColumn( "xor" ), intColumn( "prefix" ) );
		final ChaosKey theCertKey;
		// Spec-file |-->
		if ( aSpecFile != null ) {
			try ( CsvRecordReader<?> theCsvReader = new CsvRecordReader<>( theHeader, aSpecFile, Delimiter.CSV.getChar() ) ) {
				theCsvReader.readHeader();
				theCertKey = theCsvReader.nextType( ChaosKeyLine.class ).toChaosKey( theCsvReader );
			}
		}
		// Spec-file <--|
		else {
			theCertKey = ChaosKey.createRndKeyChain( aChainLength, aChaosMode, ChaosMode.NONE );
		}

		String theKeyCert;
		if ( aPassword != null && aPassword.length() != 0 ) {
			theKeyCert = theCertKey.toCertificate( aPassword, aCertMode, aLineWidth );
		}
		else {
			theKeyCert = theCertKey.toCertificate( aLineWidth );
		}
		if ( aCertFile == null ) {
			if ( isVerbose ) {
				LOGGER.printTail();
			}
			System.out.println();
			System.out.println( theKeyCert );
		}
		else {
			try ( BufferedOutputStream theOutStream = new BufferedOutputStream( new FileOutputStream( aCertFile ) ) ) {
				theOutStream.write( theKeyCert.getBytes( StandardCharsets.UTF_8 ) );
			}
		}
	}

	private static void createSpecFile( File aSpecFile, boolean isVerbose ) throws IOException {
		if ( isVerbose ) {
			LOGGER.info( "Creating sample spec file <" + aSpecFile.getAbsolutePath() + "> ..." );
		}
		try ( FileOutputStream theOutputStream = new FileOutputStream( aSpecFile ); InputStream theInputStream = CliHelper.class.getResourceAsStream( "/" + DEFAULT_SPECFILE_NAME ) ) {
			theInputStream.transferTo( theOutputStream );
			theOutputStream.flush();
		}
	}

	private static void decrypt( String aText, byte[] aBytes, InputStream aInputStream, OutputStream aOutputStream, ChaosKey aChaosKey, boolean isHex, BaseMetricsConfig aBaseMetrics, boolean isVerbose ) throws DecryptionException, IOException {
		if ( ( aText != null && aText.length() != 0 ) || ( aBytes != null && aBytes.length != 0 ) ) {
			decrypt( aText, aBytes, aOutputStream, aChaosKey, aBaseMetrics, isHex, isVerbose );
		}
		else {
			if ( aBaseMetrics != null ) {
				aInputStream = new BaseDecoderInputStream( aInputStream, aBaseMetrics );
			}
			decryptStream( aInputStream, aOutputStream, aChaosKey, isVerbose );
		}
	}

	//	private static void decrypt( String aText, byte[] aBytes, ChaosKey aChaosKey, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerbose ) throws DecryptionException {
	//		if ( aChaosKey.getOptions().isSalted() && isVerbose ) LOGGER.warn( "A mode <" + aChaosKey.getOptions().toString() + "> with salting was provided, though currently salting a text or bytes argument is not supported (please use the file or stream based operations), continuing without salting!" );
	//		byte[] theMessage;
	//		if ( aBaseMetrics != null ) {
	//			theMessage = new BaseBuilder().withBaseMetrics( aBaseMetrics ).toDecodedData( aText );
	//		}
	//		else if ( aBytes != null ) {
	//			theMessage = aBytes;
	//		}
	//		else {
	//			theMessage = aText.getBytes();
	//		}
	//		byte[] theDecrypted = new ChaosDecrypter( aChaosKey ).toDecrypted( theMessage );
	//		String theResult = isHex ? "{ " + NumericalUtility.toHexString( ", ", theDecrypted ) + " }" : new String( theDecrypted );
	//		if ( isVerbose ) {
	//			LOGGER.info( "Decrypted := " + theResult );
	//		}
	//		else {
	//			System.out.println( theResult );
	//		}
	//	}

	private static void decrypt( String aText, byte[] aBytes, OutputStream aOutputStream, ChaosKey aChaosKey, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerbose ) throws DecryptionException, IOException {
		if ( aChaosKey.getOptions().isSalted() && isVerbose ) {
			LOGGER.warn( "A mode <" + aChaosKey.getOptions().toString() + "> with salting was provided, though currently salting a text or bytes argument is not supported (please use the file or stream based operations), continuing without salting!" );
		}
		byte[] theMessage = aBytes;
		if ( aBaseMetrics != null ) {
			theMessage = new BaseBuilder().withBaseMetrics( aBaseMetrics ).toDecodedData( aText );
		}
		else {
			theMessage = aText.getBytes();
		}
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theMessage );
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		decryptStream( theInputStream, theOutputStream, aChaosKey, isVerbose );
		byte[] theDecrypted = theOutputStream.toByteArray();
		String theResult = null;
		if ( isHex ) {
			theResult = "{ " + NumericalUtility.toHexString( ", ", theDecrypted ) + " }";
		}
		else {
			theResult = new String( theDecrypted );
		}
		LOGGER.printTail(); // In case a warning was logged!
		PrintWriter theWriter = new PrintWriter( aOutputStream );
		theWriter.println( theResult != null ? theResult : new String( theDecrypted ) );
		theWriter.flush();
	}

	private static void decryptStream( InputStream aInputStream, OutputStream aOutputStream, ChaosKey aChaosKey, boolean isVerbose ) throws IOException {
		LOGGER.printTail(); // Also in case a warning was logged!
		try ( DecryptionInputStream theChaosInputStream = new ChaosDecryptionInputStream( new BufferedInputStream( aInputStream ), aChaosKey ) ) {
			try ( DecryptionInputStream theInputStream = theChaosInputStream ) {
				try ( OutputStream theOutputStream = new BufferedOutputStream( aOutputStream ) ) {
					theInputStream.transferTo( theOutputStream );
				}
			}
		}
	}

	private static void encrypt( String aText, byte[] aBytes, InputStream aInputStream, OutputStream aOutputStream, ChaosKey aChaosKey, boolean isVerify, boolean isHex, BaseMetricsConfig aBaseMetrics, int aLineWidth, boolean isVerbose ) throws EncryptionException, IOException {
		if ( ( aText != null && aText.length() != 0 ) || ( aBytes != null && aBytes.length != 0 ) ) {
			encrypt( aText, aBytes, aOutputStream, aChaosKey, aBaseMetrics, isHex, isVerify, isVerbose );
		}
		else {
			if ( aBaseMetrics != null ) {
				if ( aLineWidth != -1 ) {
					aOutputStream = new LineBreakOutputStream( aOutputStream, aLineWidth );
				}
				aOutputStream = new BaseEncoderOutputStream( aOutputStream, aBaseMetrics );
			}
			encryptStream( aInputStream, aOutputStream, aChaosKey, isVerify, isVerbose );
		}
	}

	//	private static void encrypt( String aText, byte[] aBytes, ChaosKey aChaosKey, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerify, boolean isVerbose ) throws EncryptionException, IOException {
	//		if ( aChaosKey.getOptions().isSalted() && isVerbose ) LOGGER.warn( "A mode <" + aChaosKey.getOptions().toString() + "> with salting was provided, though currently salting a text or bytes argument is not supported (please use the file or stream based operations), continuing without salting!" );
	//		byte[] theMessage;
	//		if ( aBytes != null ) {
	//			theMessage = aBytes;
	//		}
	//		else {
	//			theMessage = aText.getBytes();
	//		}
	//		byte[] theEncrypted = new ChaosEncrypter( aChaosKey, isVerify ).toEncrypted( theMessage );
	//		String theResult;
	//		if ( aBaseMetrics != null ) {
	//			theResult = new BaseBuilder().withBaseMetrics( aBaseMetrics ).toEncodedText( theEncrypted );
	//		}
	//		else if ( isHex ) {
	//			theResult = "{ " + NumericalUtility.toHexString( ", ", theEncrypted ) + " }";
	//		}
	//		else {
	//			theResult = new String( theEncrypted );
	//		}
	//		if ( isVerbose ) {
	//			LOGGER.info( "Decrypted := " + theResult );
	//		}
	//		else {
	//			System.out.println( theResult );
	//		}
	//	}

	private static void encrypt( String aText, byte[] aBytes, OutputStream aOutputStream, ChaosKey aChaosKey, BaseMetrics aBaseMetrics, boolean isHex, boolean isVerify, boolean isVerbose ) throws EncryptionException, IOException {
		if ( aChaosKey.getOptions().isSalted() && isVerbose ) {
			LOGGER.warn( "A mode <" + aChaosKey.getOptions().toString() + "> with salting was provided, though currently salting a text or bytes argument is not supported (please use the file or stream based operations), continuing without salting!" );
		}
		byte[] theMessage = aBytes != null ? aBytes : aText.getBytes();
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theMessage );
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		encryptStream( theInputStream, theOutputStream, aChaosKey, isVerify, isVerbose );
		final byte[] theEncrypted = theOutputStream.toByteArray();
		String theResult = null;
		if ( aBaseMetrics != null ) {
			theResult = new BaseBuilder().withBaseMetrics( aBaseMetrics ).toEncodedText( theEncrypted );
		}
		else if ( isHex ) {
			theResult = "{ " + NumericalUtility.toHexString( ", ", theEncrypted ) + " }";
		}
		else {
			theResult = new String( theEncrypted );
		}
		LOGGER.printTail(); // Also in case a warning was logged!
		PrintWriter theWriter = new PrintWriter( aOutputStream );
		if ( theResult != null ) {
			theWriter.println( theResult );
		}
		else {
			theWriter.println( theEncrypted );
		}
		theWriter.flush();

	}

	private static void encryptStream( InputStream aInputStream, OutputStream aOutputStream, ChaosKey aChaosKey, boolean isVerify, boolean isVerbose ) throws IOException {
		try ( InputStream theInputStream = new BufferedInputStream( aInputStream ) ) {
			try ( EncryptionOutputStream theChaosOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( aOutputStream ), aChaosKey, isVerify ) ) {
				try ( EncryptionOutputStream theOutputStream = theChaosOutputStream ) {
					theInputStream.transferTo( theOutputStream );
				}
			}
		}
	}

	private static InputStream toInputStream( Boolean isPasteFrom, String aInputFileName, boolean isVerbose ) throws UnsupportedFlavorException, IOException {
		InputStream theInputStream;
		if ( isPasteFrom ) {
			Clipboard theClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			theInputStream = new ByteArrayInputStream( ( (String) theClipboard.getData( DataFlavor.stringFlavor ) ).getBytes() );
		}
		else {
			theInputStream = Execution.toBootstrapStandardIn();
			if ( aInputFileName != null && aInputFileName.length() != 0 ) {
				File theInputFile = new File( aInputFileName );
				if ( isVerbose ) {
					LOGGER.info( "Input file = " + theInputFile.getAbsolutePath() );
				}
				if ( !theInputFile.exists() || !theInputFile.isFile() ) {
					throw new FileNotFoundException( "No such file \"" + aInputFileName + "\" (<" + theInputFile.getAbsolutePath() + ">) found!" );
				}
				theInputStream = new FileInputStream( theInputFile );
			}
		}
		return theInputStream;
	}

	private static OutputStream toOutputStream( Boolean isCopyTo, String aOutputFileName, boolean isVerbose ) throws FileNotFoundException {
		OutputStream theOutputStream;
		if ( isCopyTo ) {
			theOutputStream = new ClipboardOutputStream();
		}
		else {
			theOutputStream = Execution.toBootstrapStandardOut();
			if ( aOutputFileName != null && aOutputFileName.length() != 0 ) {
				File theOutputFile = new File( aOutputFileName );
				if ( isVerbose ) {
					LOGGER.info( "Output file = " + theOutputFile.getAbsolutePath() );
				}
				theOutputStream = new FileOutputStream( theOutputFile );
			}
		}
		return theOutputStream;
	}

	private static File toCertFile( String aCertFileName, boolean isVerbose ) {
		final File theCertFile = ( aCertFileName != null && aCertFileName.length() != 0 ) ? new File( aCertFileName ) : null;
		if ( isVerbose ) {
			if ( theCertFile != null ) {
				LOGGER.info( "Cert file = \"" + aCertFileName + "\" (<" + theCertFile.getAbsolutePath() + ">)" );
			}
		}
		return theCertFile;
	}

	private static String toSpecFileName( String aSpecFileName ) {
		return aSpecFileName != null ? aSpecFileName : DEFAULT_SPECFILE_NAME;
	}

	private static File toSpecFile( String aSpecFileName, boolean isVerbose ) {
		final File theSpecFile = ( aSpecFileName != null && aSpecFileName.length() != 0 ) ? new File( aSpecFileName ) : null;
		if ( isVerbose ) {
			if ( theSpecFile != null ) {
				LOGGER.info( "Specification file = \"" + aSpecFileName + "\" (<" + theSpecFile.getAbsolutePath() + ">)" );
			}
		}
		return theSpecFile;
	}

	private static ChaosOptions toCertMode( Boolean isPromptPassword, String aPassword, SystemContext aContext, ChaosOptions aCertMode, boolean isVerbose ) {
		final ChaosOptions theCertMode = ( aCertMode == null && ( isPromptPassword || ( aPassword != null && aPassword.length() != 0 ) || aContext != null ) ) ? DEFAULT_CHAOS_MODE : aCertMode;
		if ( isVerbose ) {
			if ( theCertMode != null ) {
				LOGGER.info( "Certificate mode = " + theCertMode.toString() );
			}

		}
		return theCertMode;
	}

	private static ChaosOptions toChaosMode( ChaosOptions aChaosMode, boolean isVerbose ) {
		aChaosMode = ( aChaosMode == null ) ? DEFAULT_CHAOS_MODE : aChaosMode;
		if ( isVerbose ) {
			if ( aChaosMode != null ) {
				LOGGER.info( "Chaos mode = " + ( aChaosMode instanceof ChaosMode ? ( (ChaosMode) aChaosMode ).name() : aChaosMode.toString() ) );
			}
		}
		return aChaosMode;
	}

	private static ChaosOptions toChaosMode( ChaosOptions aChaosMode, String aText, byte[] aBytes, boolean isPasteFrom, File aCertFile, boolean isVerbose ) {
		if ( aChaosMode == null && aCertFile == null ) {
			aChaosMode = isPasteFrom || aText != null || aBytes != null ? DEFAULT_PLAIN_MODE : DEFAULT_CHAOS_MODE;
		}
		if ( isVerbose ) {
			if ( aChaosMode != null ) {
				LOGGER.info( "Chaos mode = " + ( aChaosMode instanceof ChaosMode ? ( (ChaosMode) aChaosMode ).name() : aChaosMode.toString() ) );
			}
		}
		return aChaosMode;
	}

	private static Integer toChainLength( Integer aChainLength, File aSpecFile, boolean isVerbose ) {
		if ( aChainLength != null && aChainLength < 1 ) {
			throw new IllegalArgumentException( "The chain length <" + aChainLength + "> of argument <" + CHAIN_LENGTH_PROPERTY + "> must be at least <1> !" );
		}
		aChainLength = aChainLength == null && aSpecFile == null ? (Integer) DEFAULT_CHAIN_LENGTH : aChainLength;
		if ( isVerbose ) {
			if ( aChainLength != null ) {
				LOGGER.info( "Chain length = " + aChainLength );
			}
		}
		return aChainLength;
	}

	private static int toLineWidth( Integer aLineWidth, int aDefaultLineWidth, boolean isVerbose ) {
		if ( aLineWidth != null && aLineWidth < MIN_LINE_WIDTH ) {
			throw new IllegalArgumentException( "The line width <" + aLineWidth + "> of argument <" + LINE_WIDTH_PROPERTY + "> must be at least <" + MIN_LINE_WIDTH + "> !" );
		}
		if ( aLineWidth == null ) {
			aLineWidth = aDefaultLineWidth;
		}
		if ( isVerbose ) {
			if ( aLineWidth != null && aLineWidth != -1 ) {
				LOGGER.info( "Line width = " + aLineWidth );
			}
		}
		return aLineWidth;
	}

	private static ChaosKey toChaosKey( Boolean isPromptPassword, String aPassword, SystemContext aContext, File aCertFile, Double x0, Double a, Long s, ChaosOptions aChaosOptions, boolean isVerbose ) throws IOException {
		if ( aCertFile == null && x0 == null && a == null && s == null && aContext == null ) {
			aContext = DEFAULT_CONTEXT;
		}
		final String thePassword = ( isPromptPassword ? ( aCertFile != null ? readCertPassword( isVerbose ) : readKeyPassword( isVerbose ) ) : ( aPassword != null ) ? aPassword : ( aContext != null ? aContext.toContextString() : null ) );
		ChaosKey theChaosKey;
		if ( aCertFile != null ) {
			try {
				theChaosKey = ( thePassword != null && thePassword.length() != 0 ) ? ChaosKey.createFromCertificate( aCertFile, thePassword ) : ChaosKey.createFromCertificate( aCertFile );
			}
			catch ( IllegalArgumentException e ) {
				throw new IllegalArgumentException( "Cannot instantiate chaos key, maybe you provided a bad password or a bad cert file!", e );
			}
		}
		else if ( thePassword != null ) {
			theChaosKey = new ChaosKey( thePassword, aChaosOptions );
		}
		else {
			theChaosKey = new ChaosKey( x0, a, s, aChaosOptions );
		}
		return theChaosKey;
	}

	private static String toCertPassword( Boolean isPromptPassword, String aPassword, SystemContext aContext, boolean isVerbose ) {
		return ( isPromptPassword ? readCertPassword( isVerbose ) : ( aPassword != null ) ? aPassword : ( aContext != null ? aContext.toContextString() : null ) );
	}

	private static String readKeyPassword( boolean isVerbose ) {
		return readPassword( "Please enter password and hit [ENTER] > ", isVerbose );
	}

	private static String readCertPassword( boolean isVerbose ) {
		String thePasswd = readPassword( "Please enter certifcate password and hit [ENTER] > ", isVerbose );
		return thePasswd;
	}

	private static String readPassword( String aPrompt, boolean isVerbose ) {
		if ( isVerbose ) {
			LOGGER.printTail();
		}
		System.console().printf( aPrompt );
		final char[] theReadPassword = System.console().readPassword();
		if ( theReadPassword == null || theReadPassword.length == 0 ) {
			System.exit( ExitCode.CONTROL_C.getStatusCode() );
		}
		return new String( theReadPassword );
	}

	private static int printChaosKey( ChaosKey aChaosKey, Consumer<String> aPrinter, boolean isAlign ) {
		int length = 0;
		ChaosKey eChaosKey = aChaosKey;
		String eIndex;
		String eX0;
		String eA;
		String eS;
		String ePrefix;
		while ( eChaosKey != null ) {
			eIndex = isAlign ? HorizAlignTextBuilder.asAligned( "" + length, 4, '0', HorizAlignTextMode.RIGHT ) : "" + length;
			eX0 = isAlign ? HorizAlignTextBuilder.asAligned( "" + eChaosKey.getX0(), 20, ' ', HorizAlignTextMode.RIGHT ) : "" + eChaosKey.getX0();
			eA = isAlign ? HorizAlignTextBuilder.asAligned( "" + eChaosKey.getA(), 20, ' ', HorizAlignTextMode.RIGHT ) : "" + eChaosKey.getA();
			eS = isAlign ? HorizAlignTextBuilder.asAligned( "" + eChaosKey.getS(), 18, ' ', HorizAlignTextMode.RIGHT ) : "" + eChaosKey.getS();
			ePrefix = isAlign ? HorizAlignTextBuilder.asAligned( "" + eChaosKey.getOptions().getRndPrefixSize(), 3, ' ', HorizAlignTextMode.RIGHT ) : "" + eChaosKey.getOptions().getRndPrefixSize();
			aPrinter.accept( "Key [" + eIndex + "] = ( x0 = " + eX0 + ", a = " + eA + ", s = " + eS + ", salted = " + toBooleanValue( eChaosKey.getOptions().isSalted() ) + ", mutate = " + toBooleanValue( eChaosKey.getOptions().isMutateS() ) + ", xor = " + toBooleanValue( eChaosKey.getOptions().isXorNext() ) + ", prefix = " + ePrefix + " )" );
			eChaosKey = eChaosKey.getChild();
			length++;
		}
		return length;
	}

	private static String toBooleanText( Boolean isTrue ) {
		return isTrue ? "YES" : "NO";
	}

	private static String toBooleanValue( Boolean isTrue ) {
		return isTrue ? "1" : "0";
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class ChaosKeyLine {

		private static final String RND_LITERAL = "rnd";

		public String x0;
		public String a;
		public String s;
		boolean isSalted;
		boolean isMutate;
		boolean isXor;
		short prefix;

		public String getX0() {
			return x0;
		}

		public void setX0( String aX0 ) {
			x0 = aX0;
		}

		public String getA() {
			return a;
		}

		public void setA( String aA ) {
			a = aA;
		}

		public String getS() {
			return s;
		}

		public void setS( String aS ) {
			s = aS;
		}

		public boolean isSalted() {
			return isSalted;
		}

		public void setSalted( boolean aIsSalted ) {
			isSalted = aIsSalted;
		}

		public boolean isMutate() {
			return isMutate;
		}

		public void setMutate( boolean aIsMmutate ) {
			isMutate = aIsMmutate;
		}

		public boolean isXor() {
			return isXor;
		}

		public void setXor( boolean aIsXor ) {
			isXor = aIsXor;
		}

		public int getPrefix() {
			return prefix;
		}

		public void setPrefix( short aPrefix ) {
			prefix = aPrefix;
		}

		public ChaosKey toChaosKey( CsvRecordReader<?> aCsvReader ) {
			ChaosKey theRndKey = ChaosKey.createRndKey();
			double x0 = RND_LITERAL.equalsIgnoreCase( this.x0 ) ? theRndKey.getX0() : Double.valueOf( this.x0 );
			double a = RND_LITERAL.equalsIgnoreCase( this.a ) ? theRndKey.getA() : Double.valueOf( this.a );
			long s = RND_LITERAL.equalsIgnoreCase( this.s ) ? theRndKey.getS() : Long.valueOf( this.s );
			return new ChaosKey( x0, a, s, new ChaosOptionsImpl( isMutate, isXor, isSalted, prefix ), aCsvReader.hasNext() ? aCsvReader.nextType( ChaosKeyLine.class ).toChaosKey( aCsvReader ) : null );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return getClass().getSimpleName() + " [x0=" + x0 + ", a=" + a + ", s=" + s + ", isSalted=" + isSalted + ", isMutate=" + isMutate + ", isXor=" + isXor + ", prefix=" + prefix + "]";
		}
	}
}
