<?xml version="1.0" encoding="UTF-8"?>

<!-- ===========================================================================
// FUNCODES.CLUB
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// ========================================================================= -->

<project xmlns="http://maven.apache.club/POM/4.0.0" xmlns:xsi="http://www.w3.club/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.club/POM/4.0.0 http://maven.apache.club/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- PARENT                                                              -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<parent>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-parent</artifactId>
		<version>3.4.0-SNAPSHOT</version>
		<relativePath/>
	</parent>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- ARTIFACT                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<artifactId>funcodes-chaos</artifactId>
	<groupId>club.funcodes</groupId>
	<name>${project.groupId}:${project.artifactId}</name>
	<version>1.0.1</version>
	<packaging>jar</packaging>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- META-DATA                                                           -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<url>https://bitbucket.org/funcodez/${project.artifactId}</url>
	<description>
		A command line tool for playing around with obfuscation and encryption as 
		well as decryption using the Chaos-based algorithm. Get inspired by 
		"https://bitbucket.org/funcodez".
	</description>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- PROPERTIES                                                          -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<properties>
		<main.module>club.funcodes.chaos</main.module>	
		<main.class>${main.module}.Main</main.class>
		<club.funcodes.url>http://www.funcodes.club</club.funcodes.url>
	</properties>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- LICENSES                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0</url>
			<distribution>repo</distribution>
			<comments>See "http://en.wikipedia.org/wiki/Multi-licensing"</comments>
		</license>
		<license>
			<name>GNU General Public License (GPL), Version 3.0</name>
			<url>http://www.gnu.org/licenses/gpl-3.0.html</url>
			<distribution>repo</distribution>
			<comments>See "http://en.wikipedia.org/wiki/Multi-licensing"</comments>
		</license>
		<license>
			<name>Other License Agreement</name>
			<url>${club.funcodes.url}</url>
			<distribution>manual</distribution>
			<comments>
				Please contact the copyright holding author(s) of the software 
				artifacts in question for licensing issues not being covered by 
				the above listed licenses, also regarding commercial licensing 
				models or regarding the compatibility with other open source 
				licenses.
			</comments>
		</license>
	</licenses>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- DEVELOPERS                                                          -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<developers>
		<developer>
			<name>Siegfried Steiner</name>
			<email>steiner@refcodes.org</email>
			<url>${club.funcodes.url}</url>
		</developer>
	</developers>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- SCM                                                                 -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<scm>
		<connection>scm:git:git@bitbucket.org:funcodez/${project.artifactId}.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:funcodez/${project.artifactId}.git</developerConnection>
		<url>https://bitbucket.org/funcodez/${project.artifactId}</url>
	</scm>
  	
	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- REPOSITORIES                                                        -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<repositories>
		<repository>
			<id>sonatype-nexus-snapshots</id>
			<name>Sonatype Nexus Snapshots</name>
			<url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>sonatype-nexus-releases</id>
			<name>Sonatype Nexus Releases</name>
			<url>https://s01.oss.sonatype.org/content/repositories/releases</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- DEPENDENCIES                                                        -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<dependencies>
		<dependency>
			<groupId>org.refcodes</groupId>
			<artifactId>refcodes-cli</artifactId>
		</dependency>
		<dependency>
			<groupId>org.refcodes</groupId>
			<artifactId>refcodes-archetype</artifactId>
		</dependency>
		<dependency>
			<groupId>org.refcodes</groupId>
			<artifactId>refcodes-logger-alt-async</artifactId>
		</dependency>
		<dependency>
			<groupId>org.refcodes</groupId>
			<artifactId>refcodes-logger-alt-console</artifactId>
		</dependency>
	</dependencies>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- BUILD                                                               -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<build>

		<!-- /////////////////////////////////////////////////////////////// -->
		<!-- PLUGINS                                                         -->
		<!-- /////////////////////////////////////////////////////////////// -->

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.codehaus.gmaven</groupId>
				<artifactId>groovy-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- PROFILES                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<profiles>

		<!-- FatJAR -->

		<profile>
			<id>fatjar</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-shade-plugin</artifactId>
					</plugin>
				</plugins>
			</build>
		</profile>

		<!-- GraalVM -->
		
		<profile>
			<id>native-image</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.graalvm.buildtools</groupId>
    					<artifactId>native-maven-plugin</artifactId>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>